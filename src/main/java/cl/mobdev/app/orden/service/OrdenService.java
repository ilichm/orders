package cl.mobdev.app.orden.service;

import cl.mobdev.app.orden.model.Orden;
import cl.mobdev.app.orden.repository.IOrdenRepository;
import cl.mobdev.app.orden.repository.OrdenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service
public class OrdenService implements IOrdenService{

    @Autowired
    IOrdenRepository repository;

    @Override
    public Orden crearOrden(Orden orden) {
        Orden resp = repository.crearOrden(orden);
        return resp;
    }

    @Override
    public Orden ordenPorid(Integer id){
        Orden resp = repository.ordenPorId(id);
        return resp;
    }
}
