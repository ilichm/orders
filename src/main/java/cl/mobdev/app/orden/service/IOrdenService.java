package cl.mobdev.app.orden.service;

import cl.mobdev.app.orden.model.Orden;

public interface IOrdenService {
    Orden crearOrden(Orden orden);
    Orden ordenPorid(Integer id);
}
