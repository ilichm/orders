package cl.mobdev.app.orden.controller;

import cl.mobdev.app.orden.model.Orden;
import cl.mobdev.app.orden.service.OrdenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/api/v1")
public class OrdenController {

    @Autowired
    OrdenService service;

    @PostMapping(value="/orders")
    public ResponseEntity<Orden> create(@RequestBody Orden request) {

        Orden response = service.crearOrden(request);
        return new ResponseEntity<Orden>(response,HttpStatus.CREATED);
    }

    @GetMapping(value="/orders/{id}")
    public ResponseEntity<Orden> byId(@PathVariable Integer id) {
        Orden response = service.ordenPorid(id);
        return new ResponseEntity<Orden>(response,HttpStatus.OK);
    }
}
