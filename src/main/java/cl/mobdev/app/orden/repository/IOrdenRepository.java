package cl.mobdev.app.orden.repository;

import cl.mobdev.app.orden.model.Orden;

public interface IOrdenRepository {
    Orden crearOrden(Orden req);
    Orden ordenPorId(Integer id);
}
