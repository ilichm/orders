package cl.mobdev.app.orden.repository;

import cl.mobdev.app.orden.model.Orden;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class OrdenRepository implements IOrdenRepository{

    public Orden crearOrden(Orden req) {
        Orden orden = new Orden();
        orden.setDetalle("orden_prueba");
        orden.setEstado(true);
        orden.setFecha(new Date());
        orden.setId(011);
        return orden;
    }

    public Orden ordenPorId(Integer id){
        List<Orden> lista = listaOrdenes();

        return lista.stream()
                .filter(orden -> orden.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    private List<Orden> listaOrdenes() {
        List<Orden> listaOrdenes = new ArrayList<>();
        Orden orden1 = new Orden();
        orden1.setId(1);
        orden1.setDetalle("detalle_1");
        orden1.setEstado(true);
        orden1.setFecha(new Date());

        Orden orden2 = new Orden();
        orden2.setId(2);
        orden2.setDetalle("detalle_2");
        orden2.setEstado(true);
        orden2.setFecha(new Date());

        Orden orden3 = new Orden();
        orden1.setId(3);
        orden1.setDetalle("detalle_3");
        orden1.setEstado(true);
        orden1.setFecha(new Date());

        listaOrdenes.add(orden1);
        listaOrdenes.add(orden2);
        listaOrdenes.add(orden3);

        return listaOrdenes;
    }
}
